﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;

namespace Edison
{
    partial class EdisonForm
	{

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EdisonForm));
            this.recLabel = new System.Windows.Forms.Label();
            this.recognized = new System.Windows.Forms.Label();
            this.loopLabel = new System.Windows.Forms.Label();
            this.repeatLabel = new System.Windows.Forms.Label();
            this.shuffleLabel = new System.Windows.Forms.Label();
            this.taskbarIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.playingGroupBox = new System.Windows.Forms.GroupBox();
            this.textSong = new System.Windows.Forms.Label();
            this.textAlbum = new System.Windows.Forms.Label();
            this.textArtist = new System.Windows.Forms.Label();
            this.artistLabel = new System.Windows.Forms.Label();
            this.albumLabel = new System.Windows.Forms.Label();
            this.songLabel = new System.Windows.Forms.Label();
            this.mediaPlayer = new Edison.MediaPlayer();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // recLabel
            // 
            this.recLabel.AutoSize = true;
            this.recLabel.Location = new System.Drawing.Point(16, 150);
            this.recLabel.Name = "recLabel";
            this.recLabel.Size = new System.Drawing.Size(88, 13);
            this.recLabel.TabIndex = 1;
            this.recLabel.Text = "Recognized Text";
            // 
            // recognized
            // 
            this.recognized.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.recognized.Location = new System.Drawing.Point(35, 166);
            this.recognized.Name = "recognized";
            this.recognized.Size = new System.Drawing.Size(233, 20);
            this.recognized.TabIndex = 0;
            // 
            // loopLabel
            // 
            this.loopLabel.AutoSize = true;
            this.loopLabel.Enabled = false;
            this.loopLabel.Location = new System.Drawing.Point(206, 195);
            this.loopLabel.Name = "loopLabel";
            this.loopLabel.Size = new System.Drawing.Size(31, 13);
            this.loopLabel.TabIndex = 5;
            this.loopLabel.Text = "Loop";
            // 
            // repeatLabel
            // 
            this.repeatLabel.AutoSize = true;
            this.repeatLabel.Location = new System.Drawing.Point(243, 195);
            this.repeatLabel.Name = "repeatLabel";
            this.repeatLabel.Size = new System.Drawing.Size(42, 13);
            this.repeatLabel.TabIndex = 6;
            this.repeatLabel.Text = "Repeat";
            // 
            // shuffleLabel
            // 
            this.shuffleLabel.AutoSize = true;
            this.shuffleLabel.Location = new System.Drawing.Point(160, 195);
            this.shuffleLabel.Name = "shuffleLabel";
            this.shuffleLabel.Size = new System.Drawing.Size(40, 13);
            this.shuffleLabel.TabIndex = 7;
            this.shuffleLabel.Text = "Shuffle";
            // 
            // taskbarIcon
            // 
            this.taskbarIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("taskbarIcon.Icon")));
            this.taskbarIcon.Text = "Loading...";
            this.taskbarIcon.Visible = true;
            // 
            // playingGroupBox
            // 
            this.playingGroupBox.Location = new System.Drawing.Point(12, 12);
            this.playingGroupBox.Name = "playingGroupBox";
            this.playingGroupBox.Size = new System.Drawing.Size(273, 130);
            this.playingGroupBox.TabIndex = 14;
            this.playingGroupBox.TabStop = false;
            this.playingGroupBox.Text = "Currently Playing";
            // 
            // textSong
            // 
            this.textSong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.textSong.Location = new System.Drawing.Point(35, 109);
            this.textSong.Name = "textSong";
            this.textSong.Size = new System.Drawing.Size(233, 20);
            this.textSong.TabIndex = 8;
            // 
            // textAlbum
            // 
            this.textAlbum.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.textAlbum.Location = new System.Drawing.Point(35, 76);
            this.textAlbum.Name = "textAlbum";
            this.textAlbum.Size = new System.Drawing.Size(233, 20);
            this.textAlbum.TabIndex = 9;
            // 
            // textArtist
            // 
            this.textArtist.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.textArtist.Location = new System.Drawing.Point(35, 43);
            this.textArtist.Name = "textArtist";
            this.textArtist.Size = new System.Drawing.Size(233, 20);
            this.textArtist.TabIndex = 10;
            // 
            // artistLabel
            // 
            this.artistLabel.AutoSize = true;
            this.artistLabel.Location = new System.Drawing.Point(16, 30);
            this.artistLabel.Name = "artistLabel";
            this.artistLabel.Size = new System.Drawing.Size(30, 13);
            this.artistLabel.TabIndex = 11;
            this.artistLabel.Text = "Artist";
            // 
            // albumLabel
            // 
            this.albumLabel.AutoSize = true;
            this.albumLabel.Location = new System.Drawing.Point(16, 63);
            this.albumLabel.Name = "albumLabel";
            this.albumLabel.Size = new System.Drawing.Size(36, 13);
            this.albumLabel.TabIndex = 12;
            this.albumLabel.Text = "Album";
            // 
            // songLabel
            // 
            this.songLabel.AutoSize = true;
            this.songLabel.Location = new System.Drawing.Point(16, 96);
            this.songLabel.Name = "songLabel";
            this.songLabel.Size = new System.Drawing.Size(32, 13);
            this.songLabel.TabIndex = 13;
            this.songLabel.Text = "Song";
            // 
            // mediaPlayer
            // 
            this.mediaPlayer.Enabled = true;
            this.mediaPlayer.Location = new System.Drawing.Point(-3, 209);
            this.mediaPlayer.Name = "mediaPlayer";
            this.mediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mediaPlayer.OcxState")));
            this.mediaPlayer.Size = new System.Drawing.Size(300, 10);
            this.mediaPlayer.TabIndex = 4;
            this.mediaPlayer.TabStop = false;
            this.mediaPlayer.Visible = false;
            // 
            // EdisonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 211);
            this.Controls.Add(this.songLabel);
            this.Controls.Add(this.albumLabel);
            this.Controls.Add(this.artistLabel);
            this.Controls.Add(this.textArtist);
            this.Controls.Add(this.textAlbum);
            this.Controls.Add(this.textSong);
            this.Controls.Add(this.shuffleLabel);
            this.Controls.Add(this.repeatLabel);
            this.Controls.Add(this.loopLabel);
            this.Controls.Add(this.mediaPlayer);
            this.Controls.Add(this.recLabel);
            this.Controls.Add(this.recognized);
            this.Controls.Add(this.playingGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EdisonForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edison   ";
            this.TopMost = true;
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressed);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyReleased);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosing);
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion       

        protected Label recLabel;
        private MediaPlayer mediaPlayer;
        protected Label recognized;
        private Label loopLabel;
        private Label repeatLabel;
        private Label shuffleLabel;
        private NotifyIcon taskbarIcon;
        private GroupBox playingGroupBox;
        protected Label textSong;
        protected Label textAlbum;
        protected Label textArtist;
        private Label artistLabel;
        private Label albumLabel;
        private Label songLabel;
    }


}

