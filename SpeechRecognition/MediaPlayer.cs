﻿/*
 * MediaPlayer.cs (LGPL)
 * This is an extension of AxWindowsMediaPlayer to allow direct and
 * convenient access to the players commands.
 * 
 * Copyright (c) 2008, Jesse Bowes
 * 
 * This file (MediaPlayer) is part of Edison, but is licensed seperately.
 * 
 * MediaPlayer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * MediaPlayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

namespace Edison
{
    class MediaPlayer : AxWMPLib.AxWindowsMediaPlayer
    {
        // State info
        private bool paused = false;
        private bool repeat = false;
        private bool commanded = false;
        private WMPLib.IWMPMedia repeatSong = null;

        public MediaPlayer()
        {
            base.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(PlayerStateChange);
        }

        /// <summary>
        /// Play current item
        /// </summary>
        public void Play()
        {
            if (repeat)
                base.Ctlcontrols.playItem(repeatSong);
            else
                base.Ctlcontrols.play();

            paused = false;
        }

        /// <summary>
        /// Pause playback
        /// </summary>
        public void Pause()
        {
            base.Ctlcontrols.pause();
            paused = true;
        }

        /// <summary>
        /// Playback paused?
        /// </summary>
        /// <returns></returns>
        public bool isPaused()
        {
            return paused;
        }

        /// <summary>
        /// Stop playback
        /// </summary>
        public void Stop()
        {
            base.Ctlcontrols.stop();
        }

        /// <summary>
        /// Skip to next song
        /// </summary>
        public void Next()
        {
            commanded = true;
            base.Ctlcontrols.next();
        }

        /// <summary>
        /// Skip to previous song
        /// </summary>
        public void Previous()
        {
            commanded = true;
            base.Ctlcontrols.previous();
        }

        /// <summary>
        /// Mute playback
        /// </summary>
        /// <param name="mute"></param>
        public void Mute(bool mute)
        {
            base.settings.mute = mute;
        }

        public bool Muted()
        {
            return base.settings.mute;
        }

        /// <summary>
        /// Shuffles current playlist
        /// </summary>
        /// <param name="shuffle"></param>
        public void Shuffle(bool shuffle)
        {
            base.settings.setMode("shuffle", shuffle);
        }

        /// <summary>
        /// Is player set to shuffle?
        /// </summary>
        /// <returns></returns>
        public bool Shuffled()
        {
            return base.settings.getMode("shuffle");
        }

        /// <summary>
        /// Loops through current playlist
        /// </summary>
        /// <param name="loop"></param>
        public void Loop(bool loop)
        {
            base.settings.setMode("loop", loop);
        }

        /// <summary>
        /// Is player set to loop?
        /// </summary>
        /// <returns></returns>
        public bool Looped()
        {
            return base.settings.getMode("loop");
        }

        /// <summary>
        /// Repeats current song only
        /// </summary>
        /// <param name="repeat"></param>
        public void Repeat(bool rep)
        {
            if (rep)
            {
                repeat = true;
                repeatSong = base.currentMedia;
            }
            else
            {
                repeat = false;
                repeatSong = null;
            }
        }

        /// <summary>
        /// Is player set to repeat?
        /// </summary>
        /// <returns></returns>
        public bool Repeated()
        {
            return repeat;
        }

        /// <summary>
        /// Increase volume
        /// </summary>
        /// <param name="increment">Amount to increase by</param>
        public void VolumeUp(int increment)
        {
            base.settings.volume = base.settings.volume + increment;
        }

        /// <summary>
        /// Decreases volume
        /// </summary>
        /// <param name="increment">Amount to decreast by</param>
        public void VolumeDown(int increment)
        {
            base.settings.volume = base.settings.volume - increment;
        }

        /// <summary>
        /// Sets volume
        /// </summary>
        /// <param name="volume">Volume</param>
        public void Volume(int volume)
        {
            base.settings.volume = volume;
        }

        /// <summary>
        /// Gets current volume
        /// </summary>
        /// <returns></returns>
        public int getVolume()
        {
            return base.settings.volume;
        }

        /// <summary>
        /// Play all songs by artist
        /// </summary>
        /// <param name="artist">Artist name</param>
        public void PlayArtist(string artist)
        {
            base.currentPlaylist = base.mediaCollection.getByAuthor(artist);
        }

        /// <summary>
        /// Play all songs by album
        /// </summary>
        /// <param name="album">Album name</param>
        public void PlayAlbum(string album)
        {
            base.currentPlaylist = base.mediaCollection.getByAlbum(album);
        }

        /// <summary>
        /// Play all songs by song name
        /// </summary>
        /// <param name="song">Song name</param>
        public void PlaySong(string song)
        {
            base.currentPlaylist = base.mediaCollection.getByName(song);
        }

        /// <summary>
        /// Play all songs by genre
        /// </summary>
        /// <param name="genre">Genre name</param>
        public void PlayGenre(string genre)
        {
            base.currentPlaylist = base.mediaCollection.getByGenre(genre);
        }

        /// <summary>
        /// Play all songs in library
        /// </summary>
        public void PlayAll()
        {
            base.currentPlaylist = base.mediaCollection.getAll();
        }

        public WMPLib.IWMPPlaylist GetAll()
        {
            return base.mediaCollection.getAll();
        }

        /// <summary>
        /// Current song artist
        /// </summary>
        /// <returns></returns>
        public string getArtist()
        {
            return base.currentMedia.getItemInfo("Artist");
        }

        /// <summary>
        /// Current song album
        /// </summary>
        /// <returns></returns>
        public string getAlbum()
        {
            return base.currentMedia.getItemInfo("Album");
        }

        /// <summary>
        /// Current Song Name
        /// </summary>
        /// <returns></returns>
        public string getSong()
        {
            return base.currentMedia.getItemInfo("Name");
        }

        /// <summary>
        /// Position in current song
        /// </summary>
        /// <returns></returns>
        public string CurrentPosition()
        {
            return base.Ctlcontrols.currentPositionString;
        }

        /// <summary>
        /// Listens for media player event change to make sure repeat doesn't let the songs progress
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void PlayerStateChange(System.Object Sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 9) // mediaTransitioned
            {
                if (repeat && !commanded)
                {
                    base.Ctlcontrols.playItem(repeatSong);
                }

                commanded = false;
            }
        }
    }
}
