﻿/*
 * Program.cs (GPL)
 * This is the main starting point for Edison.
 * 
 * Copyright (c) 2008, Jesse Bowes, Daniel Cousineau, Daniel Duke
 * 
 * This file is part of Edison.
 * 
 * Edison is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * Edison is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using Edison.settings;

namespace Edison
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
			init();

            Settings s = loadSettings();

            if (s != null)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new EdisonForm(s));
            }
        }

		public static void init()
		{
			Speech.init();
		}

        /// <summary>
        /// Loads settings from "Settings.xml".  If it doesn't exists or there is an error, 
        /// prompt to exit or regenerate.
        /// </summary>
        /// <returns></returns>
        public static Settings loadSettings()
        {
            Settings s = new Settings();
            XmlSerializer x = new XmlSerializer(s.GetType());
            TextReader tr;

            bool fail = false;
            string message = "";

            try
            {
                tr = new StreamReader("Settings.xml");

                try
                {
                    s = (Settings)x.Deserialize(tr);
                }
                catch (Exception e)
                { 
                    fail = true;
                    message = e.Message;
                }

                tr.Close();
            }
            catch (Exception e)
            {
                fail = true;
                message = e.Message;
            }

            if (fail)
            {
                DialogResult dr = MessageBox.Show(message + "\r\n\r\n Regenerate Settings.xml?", "Error", MessageBoxButtons.YesNo);
                if (dr.ToString().Equals("Yes"))
                {
                    TextWriter w = new StreamWriter("Settings.xml");
                    x.Serialize(w, s);
                    w.Close();
                }
                else
                {
                    Environment.Exit(1);                   
                }
            }        

            return s;
        }
    }
}
