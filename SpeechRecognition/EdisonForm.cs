﻿/*
 * EdisonForm.cs (GPL)
 * Main Form for Edison.
 * 
 * Copyright (c) 2008, Jesse Bowes, Daniel Cousineau, Daniel Duke
 * 
 * This file is part of Edison.
 * 
 * Edison is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * Edison is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using CrystalFontz;
using Edison.settings;
using SpeechLib;
using Tools;

namespace Edison
{
    public partial class EdisonForm : Form
    {
        private SpeechLib.SpSharedRecoContext objRecoContext = null;
        private SpeechLib.ISpeechRecoGrammar grammar = null;

        private LCDPanel panel;
        
        private bool recogEnabled = false;

        // settings
        private Settings settings;

        private int volume = 0;

        private Icon readyIcon = new Icon("icon.ico");
        private Icon loadingIcon = new Icon("notready.ico");

        private string grammarDir = "grammars\\";

        private string playerXML = "command.xml";
        private string albumXML  = "album.xml";
        private string artistXML = "artist.xml";
        private string songXML   = "song.xml";

        private string currentXML = "";

        private const short COMMAND = 0;
        private const short SONG = 1;
        private const short ALBUM = 2;
        private const short ARTIST = 3;

        private short listenMode = COMMAND;

        private System.Timers.Timer recogTimer;

        private System.Timers.Timer songTimeTimer = new System.Timers.Timer(1000);

        public EdisonForm(Settings s)
        {
            settings = s;
            InitializeComponent();
            initSettings();
            initDisplay();
            generateXML();
            initSpeech();
            initStatus();
            initFormEvents();
            initIcons();
            readyDisplay();
            speak("Edison is Online");
        }

        private void initSettings()
        {
            recogTimer = new System.Timers.Timer(settings.system.commandTimeout);

            // Player
            mediaPlayer.Volume(settings.player.volume);
            volume = settings.player.volume;
            mediaPlayer.Shuffle(settings.player.shuffle);
            mediaPlayer.Loop(settings.player.loop);
            mediaPlayer.Repeat(settings.player.repeat);
        }

        private void updateWindow()
        {
            string artist = "";
            string album = "";
            string song = "";

            if (mediaPlayer.currentMedia != null)
            {
                artist = mediaPlayer.getArtist();
                album = mediaPlayer.getAlbum();
                song = mediaPlayer.getSong();
            }

            textArtist.Text = artist;
            textAlbum.Text = album;
            textSong.Text = song;
        }

        private void initDisplay()
        {
            try
            {
                panel = new LCDPanel(LCDType.CrystalFontz634, settings.screen.port, 19200, settings.screen.scrollDelay);
                panel.Write(Command.Reboot);
                panel.Write(Command.RestoreDisplay);
                panel.SetBacklight(settings.screen.backlight);
                panel.SetContrast(settings.screen.contrast);
                panel.StopMarquee();
                panel.Write(Command.HideCursor);
                panel.Write(Command.ClearDisplay);
                panel.Write(Command.WrapOff);
                panel.Write(Command.ScrollOff);
                panel.WriteToRow(Application.ProductName, 0, TextAlignment.Centered);
                panel.WriteToRow(Application.ProductVersion, 2, TextAlignment.Centered);
                panel.WriteToRow("Loading...", 3, TextAlignment.Centered);
            }
            catch (IOException) { }
        }

        public void readyDisplay()
        {
            if (panel != null)
            {
                panel.Write(Command.ClearDisplay);
                panel.WriteToRow("Ready", 2, TextAlignment.Centered);                
            }
        }

        public void fillDisplay()
        {
            songTimeTimer.Stop();
            if (panel != null)
            {
                string artist = "";
                string album = "";
                string song = "";

                if (mediaPlayer.currentMedia != null)
                {
                    artist = mediaPlayer.getArtist();
                    album = mediaPlayer.getAlbum();
                    song = mediaPlayer.getSong();
                }

                panel.ScrollLine(song, 0);
                panel.ScrollLine(artist, 1);
                panel.ScrollLine(album, 2);

                updateDisplayTime();
            }
            songTimeTimer.Start();
        }

        public void updateDisplayTime()
        {
            songTimeTimer.Stop();
            if (panel != null)
            {
                string current = currentXML.Remove(currentXML.Length-4) + "?";
                char first = Char.ToUpper(current[0]);
                current = first + current.Substring(1);
                panel.WriteToRow(current, 3, TextAlignment.Centered);
                panel.WriteToRow(mediaPlayer.CurrentPosition(), 3, TextAlignment.Left, false);

                if (mediaPlayer.Shuffled())
                {
                    panel.SetCursorPosition(15, 3);
                    panel.Write("S");
                }

                if (mediaPlayer.Looped())
                {
                    panel.SetCursorPosition(17, 3);
                    panel.Write("L");
                }

                if (mediaPlayer.Repeated())
                {
                    panel.SetCursorPosition(19, 3);
                    panel.Write("R");
                }
            }
            Thread.Sleep(50);
            songTimeTimer.Start();
        }

        private void initSpeech()
        {
            // Get an insance of RecoContext. I am using the shared RecoContext.
            objRecoContext = new SpeechLib.SpSharedRecoContext();

            // Assign a eventhandler for the Recognition Event.
            objRecoContext.Recognition += new _ISpeechRecoContextEvents_RecognitionEventHandler(Reco_Event);

            //Creating an instance of the grammer object.
            grammar = objRecoContext.CreateGrammar(0);

            loadGrammar(playerXML);
        }

        private void initStatus()
        {
            recogEnabled = false;
            volume = mediaPlayer.getVolume();
            shuffleLabel.Enabled = mediaPlayer.Shuffled(); ;
            loopLabel.Enabled = mediaPlayer.Looped();
            repeatLabel.Enabled = mediaPlayer.Repeated();
        }

        private void initFormEvents()
        {
            this.KeyPress += new KeyPressEventHandler(KeyPressed);
            this.KeyUp += new KeyEventHandler(KeyReleased);
            this.recogTimer.Elapsed += new ElapsedEventHandler(OnTimer);
            mediaPlayer.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(PlayerStateChange);
            this.songTimeTimer.Elapsed += new ElapsedEventHandler(OnSongTimeTimer);
        }

        private void initIcons()
        {
            taskbarIcon.Text = "Edison";
            taskbarIcon.Icon = readyIcon;
        }

        private void generateXML()
        {
            Console.WriteLine("Generating Grammar Files");

            FileStream artistFile = new FileStream(grammarDir + artistXML, FileMode.Create, FileAccess.Write);
            StreamWriter artistWriter = new StreamWriter(artistFile);

            FileStream albumFile = new FileStream(grammarDir + albumXML, FileMode.Create, FileAccess.Write);
            StreamWriter albumWriter = new StreamWriter(albumFile);

            FileStream songFile = new FileStream(grammarDir + songXML, FileMode.Create, FileAccess.Write);
            StreamWriter songWriter = new StreamWriter(songFile);

            WMPLib.IWMPPlaylist list = mediaPlayer.GetAll();

            SortedList<string, ListItem> artistList = new SortedList<string, ListItem>();
            SortedList<string, ListItem> albumList = new SortedList<string, ListItem>();
            SortedList<string, ListItem> songList = new SortedList<string, ListItem>();

            writeHeading(artistWriter);
            writeHeading(albumWriter);
            writeHeading(songWriter);

            for (int i = 0; i < list.count; i++)
            {
                string album = list.get_Item(i).getItemInfo("Album").ToLower().Trim();
                string artist = list.get_Item(i).getItemInfo("Artist").ToLower().Trim();
                string song = list.get_Item(i).getItemInfo("Name").ToLower().Trim();                       


                string cleanalbum = cleanString(album);
                string cleanartist = cleanString(artist);
                string cleansong = cleanString(song);

                ArrayList albumOptional = TextTool.GetParenthasized(album);
                ArrayList artistOptional = TextTool.GetParenthasized(artist);
                ArrayList songOptional = TextTool.GetParenthasized(song);


                // must have a valid album, artist, name to be a song, this weeds out the radio stations built
                // into wmp.
                if (cleanalbum != "" && cleanartist != "" && cleansong != "")
                {
                    try
                    {
                        artistList.Add(cleanartist, new ListItem(artist, cleanartist,artistOptional));
                    }
                    catch (ArgumentException)
                    {
                        //Console.WriteLine(artist + " already listed.");
                    }

                    try
                    {
                        //albumList.Add(cleanalbum, album);
                        albumList.Add(cleanalbum, new ListItem(album, cleanalbum, albumOptional));
                    }
                    catch (ArgumentException)
                    {
                        //Console.WriteLine(album + " already listed.");
                    }

                    try
                    {
                        //songList.Add(cleansong, song);
                        songList.Add(cleansong, new ListItem(song, cleansong, songOptional));
                    }
                    catch (ArgumentException)
                    {
                        //Console.WriteLine(song + " already listed.");
                    }
                }
            }

            foreach (KeyValuePair<string, ListItem> kvp in artistList)
            {
                if (kvp.Key != "")
                    writeElement(artistWriter, kvp.Key, kvp.Value);
            }

            foreach (KeyValuePair<string, ListItem> kvp in albumList)
            {
                if (kvp.Key != "")
                    writeElement(albumWriter, kvp.Key, kvp.Value);
            }

            foreach (KeyValuePair<string, ListItem> kvp in songList)
            {
                if (kvp.Key != "")
                    writeElement(songWriter, kvp.Key, kvp.Value);
            }

            writeFooter(artistWriter);
            writeFooter(albumWriter);
            writeFooter(songWriter);

            artistWriter.Close();
            albumWriter.Close();
            songWriter.Close();

            Console.WriteLine("Done Generating Grammar Files");
        }

        private void writeHeading(StreamWriter writer)
        {
            writer.WriteLine("<!-- THIS FILE IS AUTOGENERATED - DO NOT EDIT -->\r\n\r\n"
                + "<!-- The grammar tag surrounds the entire CFG description\r\n"
                + "Specify the language of the grammar as English-American ('409') -->\r\n"
                + "\r\n"
                + "<!-- +word = high confidence required, -word = low confidence required\r\n"
                + "<P> is required</P>   <O> is Optional </o> <O>...</O> allows for \r\n"
                + "random optional words   -->\r\n\r\n"
                + "<GRAMMAR LANGID=\"409\">\r\n"
                + "  ");
        }

        private void writeElement(StreamWriter writer, string speech, ListItem item)
        {
            string id = item.id;
            id = TextTool.HTMLize(id);

            writer.WriteLine(
                "  <RULE NAME=\"" + id + "\" TOPLEVEL=\"ACTIVE\">\r\n"
                + "    <P>" + speech + "</P>");

            for (int i = 0; i < item.optional.Count; i++)
            {
                string opt = (string)item.optional[i];
                opt = cleanString(opt);

                writer.WriteLine("    <O>" + opt + "</O>");
            }

            writer.WriteLine("  </RULE>\r\n") ;
        }

        private void writeFooter(StreamWriter writer)
        {
            writer.WriteLine("<!-- End of Grammar definition -->\r\n"
                + "</GRAMMAR>");
        }

        private string cleanString(string str)
        {
            // some special cases
            str = TextTool.UnicodeToAscii(str);
            str = TextTool.RemoveParenthesised(str);
            str = str.Replace("-", " ");
            
            char[] ary = str.ToCharArray();

            for (int i = 0; i < ary.Length; i++)
            {
                if (ary[i] > 'z' || (ary[i] < 'a' && ary[i] != ' ' && (ary[i] < '0' || ary[i] > '9')))
                    ary[i] = '#';
            }

            String s = new String(ary);

            s = s.Replace("#", "");

            s = TextTool.OrdinalNumToString(s);

            return s;
        }

        private void loadGrammar(string FileName)
        {
            string fn = String.Concat(grammarDir, FileName);
            try
            {
                grammar.CmdLoadFromFile(fn, SpeechLib.SpeechLoadOption.SLODynamic);
                grammar.CmdSetRuleIdState(0, SpeechRuleState.SGDSActive);
                currentXML = FileName;
            }
            catch
            {
                Console.WriteLine("Error Loading Grammar: " + FileName);
                return;
            }

            Console.WriteLine("Grammar Loaded: " + FileName);
        }

        // Text to Speech
        private void speak(string s)
        {
            bool temp = recogEnabled;
            recogEnabled = false;
            SpVoice voice = new SpVoice();
            voice.Speak(s, SpeechVoiceSpeakFlags.SVSFDefault);
            recogEnabled = temp;
        }

        private void KeyPressed(Object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                if (!recogEnabled)
                    mediaPlayer.Volume(mediaPlayer.getVolume() / 4);

                recogEnabled = true;
            }
        }

        private void PlayerStateChange(Object Sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 3) // playing
            {
                panel.ResetRowScroll();
            }
            updateWindow();
            fillDisplay();
        }

        private void KeyReleased(Object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 32)
            {
                mediaPlayer.Volume(volume);
                recogEnabled = false;
            }
        }

        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            recogTimer.Stop();
            songTimeTimer.Stop();

            if (panel != null)
            {
                panel.Write(Command.ClearDisplay);
                Thread.Sleep(1000);
                panel.Close();
            }
        }

        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            recogTimer.Stop();
            Console.Write("Recognize Timeout: ");
            loadGrammar(playerXML);
        }

        private void OnSongTimeTimer(object sender, ElapsedEventArgs e)
        {
            updateDisplayTime();
        }

        private void Reco_Event(int StreamNumber, object StreamPosition, SpeechRecognitionType RecognitionType, ISpeechRecoResult Result)
        {
            if (recogEnabled)
            {              
                recognized.Text = Result.PhraseInfo.GetText(0, -1, true);

                string said = recognized.Text;

                string phrase = Result.PhraseInfo.Rule.Name;


                if (listenMode == COMMAND)
                {
                    // These cases come from playerXML, artistXML, and albumXML
                    switch (phrase)
                    {
                        //generic
                        case "mainmenu":
                            speak("Main menu");
                            loadGrammar(playerXML);
                            break;

                        // commands
                        case "playsong":
                            listenMode = SONG;
                            loadGrammar(songXML);
                            recogTimer.Start();
                            break;

                        case "playartist":
                            listenMode = ARTIST;
                            loadGrammar(artistXML); 
                            recogTimer.Start();
                            break;

                        case "playalbum":
                            listenMode = ALBUM;
                            loadGrammar(albumXML);
                            recogTimer.Start();
                            break;

                        case "playall":
                            mediaPlayer.PlayAll();
                            break;


                        case "stop":
                            mediaPlayer.Stop();
                            break;

                        case "previous":
                            mediaPlayer.Previous();
                            break;

                        case "next":
                            mediaPlayer.Next();
                            break;

                        case "play":
                            mediaPlayer.Play();
                            break;

                        case "pause":
                            mediaPlayer.Pause();
                            break;

                        case "mute":
                            mediaPlayer.Mute(!mediaPlayer.Muted());
                            break;

                        case "volumeup":
                            volume = volume + 25;
                            break;

                        case "volumedown":
                            volume = volume - 25;
                            break;

                        case "random":
                        case "shuffle":
                            mediaPlayer.Shuffle(!mediaPlayer.Shuffled());
                            shuffleLabel.Enabled = mediaPlayer.Shuffled();
                            break;

                        case "loop":
                            mediaPlayer.Loop(!mediaPlayer.Looped());
                            loopLabel.Enabled = mediaPlayer.Looped();
                            break;

                        case "repeat":
                            mediaPlayer.Repeat(!mediaPlayer.Repeated());
                            repeatLabel.Enabled = mediaPlayer.Repeated();
                            break;
                    }
                }
                else if (listenMode == ALBUM)
                {
                    mediaPlayer.PlayAlbum(phrase);
                    listenMode = COMMAND;
                    loadGrammar(playerXML);
                    recogTimer.Stop();
                }
                else if (listenMode == ARTIST)
                {
                    mediaPlayer.PlayArtist(phrase);
                    listenMode = COMMAND;
                    loadGrammar(playerXML);
                    recogTimer.Stop();
                }
                else if (listenMode == SONG)
                {
                    mediaPlayer.PlaySong(phrase);
                    listenMode = COMMAND;
                    loadGrammar(playerXML);
                    recogTimer.Stop();
                }

                updateDisplayTime();
            }
        }
    }
}
