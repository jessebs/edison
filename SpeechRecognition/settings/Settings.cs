﻿/*
 * Settings.cs (GPL)
 * General settings for Edison.
 * 
 * Copyright (c) 2008, Jesse Bowes
 * 
 * This file is part of Edison.
 * 
 * Edison is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * Edison is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

namespace Edison.settings
{
    public class Settings
    {
        public int commandTimeout = 5000;
        public Screen screen;
        public Player player;
        public System system;

        public Settings()
        {
            screen = new Screen();
            player = new Player();
            system = new System();
        }
    }
}
