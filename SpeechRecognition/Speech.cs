﻿/*
 * Speech.cs (GPL)
 * Minor initializations for Edison.
 * 
 * Copyright (c) 2008, Jesse Bowes, Daniel Cousineau, Daniel Duke
 * 
 * This file is part of Edison.
 * 
 * Edison is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * Edison is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using SpeechLib;

namespace Edison
{
	class Speech
	{
		static SpVoice voice;

		public static void init()
		{
			Speech.voice = new SpVoice();
		}

		public static void Speak(string script)
		{
			Speech.voice.Speak(script, SpeechVoiceSpeakFlags.SVSFDefault);
		}
	}
}
