﻿/*
 * LCDPanel.cs (LGPL)
 * This is a C# software interface to CrystalFontz LCD screens.
 * 
 * Copyright (c) 2006, Scott Hanselman
 * Copyright (c) 2008, Jesse Bowes
 * 
 * This file (LCDPanel) is part of Edison, but is licensed seperately.
 * 
 * LCDPanel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * LCDPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Timers;
using Tools;

namespace CrystalFontz
{
    public enum LCDType
    {
        CrystalFontz632,
        CrystalFontz634,
    }

    public class LCDPanel : IDisposable
    {
        private SerialPort port;
        private LCDType type;

        private int[] rowScroll = new int[4];
        private ScrollStatus[] needScroll = new ScrollStatus[4];

        private string[] lines = new string[4];

        private System.Timers.Timer displayTimer;

        public LCDPanel(LCDType lcdType, string portNumber, int baud, int scrollspeed)
        {
            type = lcdType;
            port = new SerialPort(portNumber, baud);
            port.Open();
            displayTimer = new System.Timers.Timer(scrollspeed);
            displayTimer.Elapsed += new ElapsedEventHandler(OnDisplayTimer);
            displayTimer.Start();
        }

        void IDisposable.Dispose()
        {
            Close();
        }

        public void Close()
        {
            displayTimer.Stop();
            if (port != null && port.IsOpen == true)
            {
                port.Close();
            }
        }

        public void Write(string text)
        {    
            displayTimer.Stop();

            text = TextTool.UnicodeToAscii(text);
            port.Write(text);
            Trace.Write(text);

            displayTimer.Start();
        }

        public void Write(string[] lines)
        {
            foreach (string line in lines)
            {
                Write(line);
                Write(Command.LineFeed);
                Write(Command.CarriageReturn);
            }
        }

        public void WriteToRow(string text, int row)
        {
            WriteToRow(text, row, TextAlignment.Left, true);
        }

        public void WriteToRow(string text, int row, TextAlignment alignment)
        {
            WriteToRow(text, row, alignment, true);
        }

        public int MaxWidth
        {
            get { return (type == LCDType.CrystalFontz632 ? 16 : 20); }
        }

        public void WriteToRow(string text, int row, TextAlignment alignment, bool clearRow)
        {
            lock (this)
            {
                validateRowAndColumn(0, row);

                if (clearRow)
                {
                    SetCursorPosition(0, row);
                    Write(new string(' ', MaxWidth));
                }
                SetCursorPosition(0, row);
                switch (alignment)
                {
                    case TextAlignment.Right:
                        Write(text.PadLeft(MaxWidth));
                        break;
                    case TextAlignment.Centered:
                        string newtext = text;
                        int padding = (MaxWidth - newtext.Length);
                        newtext = newtext.PadLeft(newtext.Length + padding / 2);
                        newtext = newtext.PadRight(MaxWidth);
                        Write(newtext);
                        break;
                    default:
                        Write(text);
                        break;
                }

            }
        }

        public void Write(byte[] bytes)
        {
            displayTimer.Stop();

            if (bytes == null) throw new ArgumentNullException("bytes");

            try
            {
                port.Write(bytes, 0, bytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            displayTimer.Start();
        }

        public void Write(byte aByte)
        {
            Write(new byte[] { aByte });
        }

        public void Write(int anInt)
        {
            Write((byte)anInt);
        }

        public void Write(Command aCommand)
        {
            bool ok = false;
            while (!ok)
            {
                lock (this)
                {
                    Write((byte)aCommand);
                    ok = true;
                }
            }
        }

        public void Write(HorizontalGraphStyle aStyle)
        {
            Write((byte)aStyle);
        }

        public void SetContrast(int level)
        {
            if (level < 0 || level > 100) throw new ArgumentOutOfRangeException("Contrast Level must be between: 0 = very light and 100 = very dark");
            Write(Command.ContrastControl);
            Write(level);
        }

        public void SetBacklight(int level)
        {
            if (level < 0 || level > 100) throw new ArgumentOutOfRangeException("Backlight Level must be between: 0 = OFF and 100 = ON");
            Write(Command.BacklightControl);
            Write(level);
        }

        public void SetCursorPosition(int column, int row)
        {
            validateRowAndColumn(column, row);
            Write(Command.SetCursorPosition);
            Write(column);
            Write(row);
        }

        private void validateRowAndColumn(int column, int row)
        {
            if (type == LCDType.CrystalFontz632)
            {
                if (column < 0 || column > 15) throw new ArgumentOutOfRangeException("CrystalFontz632 is 16x12, so column must be between 0 and 15");
                if (row < 0 || row > 1) throw new ArgumentOutOfRangeException("CrystalFontz632 is 16x12, so row must be between 0 and 1");
            }
            if (type == LCDType.CrystalFontz634)
            {
                if (column < 0 || column > 19) throw new ArgumentOutOfRangeException("CrystalFontz634 is 20x4, so column must be between 0 and 19");
                if (row < 0 || row > 3) throw new ArgumentOutOfRangeException("CrystalFontz634 is 20x4, so row must be between 0 and 3");
            }
        }


        public void ShowHorizontalBarGraph(HorizontalGraphStyle style, int startColumn, int endColumn, int lengthInPixels, int row)
        {
            validateRowAndColumn(startColumn, row);
            validateRowAndColumn(endColumn, row);
            if (type == LCDType.CrystalFontz632)
            {
                if (lengthInPixels > 96) throw new ArgumentOutOfRangeException("CrystalFontz632 is 16x12, so lengthInPixels must be less than 96");
                if (lengthInPixels > -96) throw new ArgumentOutOfRangeException("CrystalFontz632 is 16x12, so lengthInPixels must be greater than -96");
            }
            if (type == LCDType.CrystalFontz634)
            {
                if (lengthInPixels > 120) throw new ArgumentOutOfRangeException("CrystalFontz634 is 20x4, so lengthInPixels must be less than 120");
                if (lengthInPixels < -120) throw new ArgumentOutOfRangeException("CrystalFontz634 is 20x4, so lengthInPixels must be greater than -120");
            }
            Write(Command.HorizontalBarGraph);
            Write(1);
            Write(style);
            Write(startColumn);
            Write(endColumn);
            if (lengthInPixels < 0)
                Write(256 - lengthInPixels);
            else
                Write(lengthInPixels);
            Write(row);
        }

        public void SetMarqueeString(string marquee)
        {
            if (marquee.Length > 20) throw new ArgumentOutOfRangeException("There are a maximum 20 possible slots for marquee characters.");
            string newMarquee = marquee.PadRight(20);
            for (int i = 0; i < newMarquee.Length; i++)
            {
                Write(Command.SetScrollingMarqueeCharacters);
                Write(i);
                Write(newMarquee[i]);
            }
        }

        public void StartMarquee(int row, int pixelShift, int updateSpeed)
        {
            //validateRowAndColumn(0, row);
            if (pixelShift < 1 || pixelShift > 6) throw new ArgumentOutOfRangeException("Valid pixel shift values are 0 through 6");
            if (updateSpeed < 5 || updateSpeed > 100) throw new ArgumentOutOfRangeException("Valid update speed values are 5 (52ms) to 100 (1.042S). A good default is 32.");
            Write(Command.EnableScrollingMarquee);
            Write(row);
            Write(pixelShift);
            Write(updateSpeed);
        }

        public void StopMarquee()
        {
            Write(Command.EnableScrollingMarquee);
            Write(255);
            Write(1);
            Write(5);
        }

        public void MoveUp()
        {
            Write(27);
            Write(91);
            Write(65);
        }

        public void MoveDown()
        {
            Write(27);
            Write(91);
            Write(66);
        }

        public void MoveRight()
        {
            Write(27);
            Write(91);
            Write(67);
        }

        public void MoveLeft()
        {
            Write(27);
            Write(91);
            Write(68);
        }

        // added
        public void ScrollLine(string text, int row)
        {
            displayTimer.Stop();
            UpdateLine(text, row);
            displayTimer.Start();
        }

        private void UpdateLine(string text, int row)
        {
            if (type == LCDType.CrystalFontz632)
                if (row < 0 || row > 1)
                    throw new ArgumentOutOfRangeException("CrystalFontz632 is 16x12, so row must be between 0 and 1");

            if (type == LCDType.CrystalFontz634)
                if (row < 0 || row > 3)
                    throw new ArgumentOutOfRangeException("CrystalFontz634 is 20x4, so row must be between 0 and 3");

            lines[row] = text;

            if (rowScroll[row] >= text.Length)
            {
                rowScroll[row] = 0;
            }

            if (text.Length >= MaxWidth)
                needScroll[row] = ScrollStatus.True;

            string shorttext = "";

            if (text.Length >= MaxWidth && needScroll[row] == ScrollStatus.True)
            {
                shorttext = text.Substring(rowScroll[row], text.Length - rowScroll[row]) + "      " + text.Substring(0, rowScroll[row]);
                WriteToRow(shorttext, row);
            }
            else if (needScroll[row] == ScrollStatus.Dirty)
            {
                needScroll[row] = ScrollStatus.False;
                WriteToRow(text, row, TextAlignment.Centered);
            }

            rowScroll[row]++;   
        }

        // added
        public void ResetRowScroll()
        {
            for (int i = 0; i < rowScroll.Length; i++)
                rowScroll[i] = 0;

            for (int i = 0; i < needScroll.Length; i++)
                needScroll[i] = ScrollStatus.Dirty;
        }

        private void OnDisplayTimer(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < lines.Length; i++)
                if(lines[i] != "")
                    UpdateLine(lines[i], i);
        }
    }

    public enum TextAlignment
    {
        Left,
        Right,
        Centered
    }

    public enum HorizontalGraphStyle : byte
    {
        ThickBar = 255,
        StripedBar = 85,
        MediumCentered = 60,
        MediumLow = 15,
        MediumHigh = 240
    }

    public enum ScrollStatus : byte
    {
        False = 0,
        True = 1,
        Dirty = 3
    }


    public enum Command : byte
    {
        CursorHome = 1,
        HideDisplay = 2,
        RestoreDisplay = 3,
        HideCursor = 4,
        ShowUnderlineCursor = 5,
        ShowBlockCursor = 6,
        ShowInvertingBlockCursor = 7,
        Backspace = 8,
        LineFeed = 10,
        DeleteInPlace = 11,
        ClearDisplay = 12,
        CarriageReturn = 13,
        BacklightControl = 14, //+byte(0-100)
        ContrastControl = 15, //+byte(0-100)
        SetCursorPosition = 17, //+byte(column)+byte(row)
        HorizontalBarGraph = 18, //+graphindex,style,startcol,endcol,length,row
        ScrollOn = 19,
        ScrollOff = 20,
        SetScrollingMarqueeCharacters = 21,
        EnableScrollingMarquee = 22,
        WrapOn = 23,
        WrapOff = 24,
        Reboot = 26,
        ShowInformationScreen = 31,
    }



}

