﻿/*
 * TextTools.cs  (LGPL)
 * Various functions for text manipulation.
 * 
 * Copyright (c) 2008, Jesse Bowes
 * 
 * This file (TextTools) is part of Edison, but is licensed seperately.
 * 
 * TextTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * as published by the Free Software Foundation.
 * 
 * TextTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Edison.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

using System;
using System.Collections;

namespace Tools
{
    class TextTool
    {
        /// <summary>
        /// Transliteration from Unicode characters to Ascii Characters
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string UnicodeToAscii(String text)
        {
            //http://jrgraphix.net/research/unicode_blocks.php?block=1
            // Latin-1 Supplement
            text = text.Replace('À', 'A');
            text = text.Replace('Á', 'A');
            text = text.Replace('Ã', 'A');
            text = text.Replace('Ä', 'A');  	
            text = text.Replace('Å', 'A');
            text = text.Replace("Æ", "AE");
            text = text.Replace("æ", "ae");
            text = text.Replace('È', 'E');  	
            text = text.Replace('É', 'E');  	
            text = text.Replace('Ê', 'E');  	
            text = text.Replace('Ë', 'E');  	
            text = text.Replace('Ì', 'I');  	
            text = text.Replace('Í', 'I');  	
            text = text.Replace('Î', 'I');
            text = text.Replace('Ï', 'I');
            text = text.Replace('Ñ', 'N');                
            text = text.Replace('Ò', 'O');
            text = text.Replace('Ó', 'O');
            text = text.Replace('Ô', 'O');
            text = text.Replace('Õ', 'O');
            text = text.Replace('Ö', 'O');
            text = text.Replace('Ù', 'U');
            text = text.Replace('Ú', 'U');
            text = text.Replace('Û', 'U');
            text = text.Replace('Ü', 'U');
            text = text.Replace('Ý', 'Y');
            text = text.Replace('à', 'a');
            text = text.Replace('á', 'a');
            text = text.Replace('â', 'a');
            text = text.Replace('ã', 'a');
            text = text.Replace('ä', 'a');
            text = text.Replace('å', 'a');
            text = text.Replace('ç', 'c');
            text = text.Replace('é', 'e');
            text = text.Replace('è', 'e');
            text = text.Replace('ê', 'e');
            text = text.Replace('ë', 'e');
            text = text.Replace('ì', 'i');
            text = text.Replace('í', 'i');
            text = text.Replace('î', 'i');
            text = text.Replace('ï', 'i');
            text = text.Replace('ñ', 'n');
            text = text.Replace('ò', 'o');
            text = text.Replace('ó', 'o');
            text = text.Replace('ô', 'o');
            text = text.Replace('õ', 'o');
            text = text.Replace('ö', 'o');
            text = text.Replace('ù', 'u');
            text = text.Replace('ú', 'u');
            text = text.Replace('û', 'u');
            text = text.Replace('ü', 'u');
            text = text.Replace('ý', 'y');
            text = text.Replace('ÿ', 'y');

            return text;
        }

        /// <summary>
        /// Convert a string to an html safe version of the string.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string HTMLize(String text)
        {
            text = text.Replace("&", "&amp;");
            text = text.Replace("\"", "&quot;");
            text = text.Replace("<", "&lt;");
            text = text.Replace(">", "&gt;");
            text = text.Replace("–", "&ndash;");
            text = text.Replace("—", "&mdash;");
            text = text.Replace("¡", "&iexcl;");
            text = text.Replace("¿", "&iquest;");
            text = text.Replace("«", "&laquo;");
            text = text.Replace("»", "&raquo;");

            return text;
        }

        /// <summary>
        /// Convert ordinal numbers to ordinal words (1st -> first)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string OrdinalNumToString(String text)
        {
            // HACK: This is a horrible way to do this.
            text = text.Replace("1st", "first");
            text = text.Replace("2nd", "second");
            text = text.Replace("3rd", "third");
            text = text.Replace("4th", "fourth");
            text = text.Replace("5th", "fifth");
            text = text.Replace("6th", "sixth");
            text = text.Replace("7th", "seventh");
            text = text.Replace("8th", "eighth");
            text = text.Replace("9th", "nineth");
            text = text.Replace("10th", "tenth");
            
            return text;
        }

        /// <summary>
        /// Remove strings located between parenthesis.  D(uc)k -> Dk.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveParenthesised(String text)
        {
            while(text.Contains("(") && text.Contains(")") && text.IndexOf('(') < text.IndexOf(')'))
            {
                int open = text.IndexOf('(');
                int closed = text.IndexOf(')');

                if (open < closed && closed > 0)
                     text = text.Remove(open, closed - open + 1);
            }

            return text.Trim();
        }

        /// <summary>
        /// Get strings that are contained within parenthesis
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ArrayList GetParenthasized(String text)
        {
            ArrayList list = new ArrayList();

            while (text.Contains("(") && text.Contains(")") && text.IndexOf('(') < text.IndexOf(')'))
            {
                int open = text.IndexOf('(');
                int closed = text.IndexOf(')');

                if (open < closed && closed > 0)
                {
                    list.Add(text.Substring(open + 1, closed - open - 1));
                    text = text.Remove(open, closed - open + 1);
                }
            }
            
            return list;
        }
    }
}
